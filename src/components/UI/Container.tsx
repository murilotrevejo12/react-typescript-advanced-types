import { ComponentPropsWithoutRef, type ElementType, ReactNode } from 'react'

type ContainerProps<T extends ElementType> = {
  as: T
  children: ReactNode
} & ComponentPropsWithoutRef<T>

export default function Container<C extends ElementType>({
  as,
  children,
  ...props
}: Readonly<ContainerProps<C>>) {
  const Component = as
  return <Component {...props}>{children}</Component>
}

import { useRef } from 'react'
import Form, { type FormHandle } from './components/UI/Form.tsx'
import Input from './components/UI/Input.tsx'
import Button from './components/UI/Button.tsx'

function App() {
  const customForm = useRef<FormHandle>(null)
  function handleSave(data: unknown) {
    // const extractedData = data as { name: string; age: string }
    if (
      !data ||
      typeof data !== 'object' ||
      !('name' in data) ||
      !('age' in data)
    ) {
      return
    }
    console.log(data)
    customForm.current?.clear()
  }
  return (
    <main>
      <Form onSave={handleSave} ref={customForm}>
        <Input label="Name" id="name" type="text" />
        <Input label="Age" id="age" type="number" />
        <p>
          <Button el="button">Save</Button>
        </p>
      </Form>
    </main>
  )
}

export default App
